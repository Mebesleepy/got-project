package se.experis;

import org.json.JSONObject;
import java.util.List;

/*
* Class to sava house information from a character to be used to get sworn m
* members to the house.
 */

public class House {
    private String name;
    private List<Object> swornMembers;

    public House(JSONObject house) {
        this.name = house.getString("name");
        this.swornMembers = house.getJSONArray("swornMembers").toList();
        }

    public String getName() {
        return name;
    }

    public List<Object> getSwornMembers() {
        return swornMembers;
    }
}

