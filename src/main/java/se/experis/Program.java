package se.experis;

import java.io.IOException;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) throws IOException {

        Scanner in = new Scanner(System.in);
        String choice;

        System.out.println("Please enter character number");
        choice = in.nextLine();
        Character character = GetData.getCharacter(choice, true);
        System.out.println(character);

        if (character.hasAllegiance()) {
            System.out.println("Do you want to list those also sworn to " + character.getName() + "'s houses?");
            System.out.println("\n1. Yes\n0. No");
            choice = in.nextLine();

            if (choice.equals("1")) {
                displaySwornMembers(character);
            }
        }

        System.out.println("Loading books...may take awhile...");
        displayBookInformation();
    }

    // display information of books depending on publisher
    public static void displayBookInformation() throws IOException {
        GetData.getBooks("Bantam Books");
    }

    // this could probably be part of a new class
    public static void displaySwornMembers(Character character) throws IOException {
        StringBuilder swornToHouses = new StringBuilder();

        for (House house : character.getHouses()) {
            String houseName = house.getName();
            for (Object url : house.getSwornMembers()) {
                swornToHouses.append(GetData.getCharacter(url));
            }
            System.out.println("------------------------" + houseName + "------------------------");
            System.out.println(swornToHouses);
        }
    }
}
