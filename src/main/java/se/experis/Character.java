package se.experis;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 *Character class for saving data for one single character who will
 * be used to access connected houses and the like
 */

public class Character {
    private String name;
    private List<Object> aliases;
    private ArrayList<House> houses = new ArrayList<>();
    private boolean hasAllegiance;

    public Character(JSONObject character) throws IOException {
        this.name = character.getString("name");
        this.aliases = character.getJSONArray("aliases").toList();

        if (name.length() < 1) {
            this.name = "No name";
        }

       if (this.aliases.isEmpty()) {
           aliases.add("No aliases");
       }

        // If there is no allegiances we skip this part.
        if (character.getJSONArray("allegiances").toList().isEmpty()) {
            this.hasAllegiance = false;
        } else {
            this.hasAllegiance = true;

            for (Object house : character.getJSONArray("allegiances").toList()) {
                this.houses.add(GetData.getHouse(house.toString()));
            }
        }
    }

    public ArrayList<House> getHouses() {
        return houses;
    }

    public String getName() {
        return name;
    }

    public boolean hasAllegiance() {
        return hasAllegiance;
    }

    public ArrayList<String> printNameOfAllegiances() {
        ArrayList<String> houses = new ArrayList<>();

        if (!this.hasAllegiance) {
            houses.add("This character has no allegiances");
            return houses;
        }

        for (House house :
                this.houses) {
            houses.add(house.getName());
        }
        return houses;
    }

    @Override
    public String toString() {
        return
                "name: " + name + "\n" +
                        "aliases: " + aliases + "\n" +
                        "allegiances: " + printNameOfAllegiances() + "\n";
    }
}
