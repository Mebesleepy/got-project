package se.experis;

import de.vandermeer.asciitable.AsciiTable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class GetData {


    // Get data for a more detailed charachter with class
    public static Character getCharacter(String url, boolean part) throws IOException {
        String partURL = "https://www.anapioficeandfire.com/api/characters/" + url;

        return new Character(getJsonObject(part ? partURL : url));
    }

    // get house data
    public static House getHouse(String url) throws IOException {
        return new House(getJsonObject(url));
    }

    // overloaded and without the step to make a charachter class-
    public static String getCharacter(Object url) throws IOException {

        JSONObject character = getJsonObject(url.toString());

        return character.get("name").toString() + "\n";

    }

    // get the books and return a arraylist of string to be looped and displayed
    public static void getBooks(String publisher) throws IOException {
        String url = "https://www.anapioficeandfire.com/api/books";


        StringBuilder stringBuilder = new StringBuilder();

        for (Object book : getJsonArray(url).toList()) {
            Map gotBook = (Map) book;

            if (gotBook.containsValue(publisher)) {

                AsciiTable at = new AsciiTable();
                at.addRule();
                at.addRow(gotBook.get("name").toString());
                at.addRule();
                //   stringBuilder.append("\n--------------- ").append(gotBook.get("name").toString()).append(" ---------------").append("\n");

                String povCharacters = gotBook.get("povCharacters").toString();

                povCharacters = povCharacters.substring(1, povCharacters.length() - 1);

                String[] povs = povCharacters.split(",");

                for (String pov : povs
                ) {
                    if (pov.length() > 0) {
                        at.addRow(GetData.getCharacter(pov));
                    } else {
                        at.addRow("Book has no point of view characters");
                    }
                }
                at.addRule();
                System.out.println(at.render());
                System.out.println("\n");
            }
        }
        System.out.println(stringBuilder);
    }

    private static StringBuffer readData(String url) throws IOException {
        StringBuffer content = new StringBuffer();
        try {
            URL Url = new URL(url);
            HttpURLConnection con = (HttpURLConnection) Url.openConnection();
            con.setRequestMethod("GET");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();
            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    // here we get a json array instead of a object
    private static JSONArray getJsonArray(String url) throws IOException {
        JSONArray jsonArray;

        jsonArray = new JSONArray(readData(url).toString());

        return jsonArray;
    }

    // should reame this to get json object
    private static JSONObject getJsonObject(String url) throws IOException {
        JSONObject jsonObject;

        jsonObject = new JSONObject(readData(url).toString());

        return jsonObject;
    }
}